import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT NOT NULL, title TEXT NOT NULL, director TEXT NOT NULL, actor TEXT NOT NULL, release_date TEXT NOT NULL, rating FLOAT NOT NULL, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

def getConnection():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(
            user=username, 
            password=password,
            host=hostname,
            database=db)
        return cnx
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(
            unix_socket=hostname, 
            user=username, 
            passwd=password, 
            db=db)
        return cnx

@app.route('/add_movie', methods=['POST'])
def insert_movie():
    cnx = getConnection()
    cur = cnx.cursor()
    f = request.form
    try:
        cur.execute("SELECT * FROM movies WHERE title=%s", (f['title'],))
        data = cur.fetchall()
        if len(data) == 0:
            cur.execute(
                "INSERT INTO movies (year, title, release_date, director, actor, rating) values (%s, %s, %s, %s, %s, %s)", 
                ( f['year'], f['title'], f['release_date'], f['director'], f['actor'], f['rating']) )

            cnx.commit()
            return render_template('index.html', message=['Movie {} successfully inserted'.format(f['title'])])
        else:
            return render_template('index.html', message=['Movie {} already exists'.format(f['title'])])
    except Exception as e:
        return render_template('index.html', message=['Movie {} could not be inserted - {}'.format(f['title'], e)])
    finally:
        cur.close()

@app.route('/update_movie', methods=['POST'])
def update_movie():
    cnx = getConnection()
    cur = cnx.cursor()
    f = request.form
    try:
        cur.execute("SELECT * FROM movies WHERE title=%s", (f['title'],))
        data = cur.fetchall()
        if len(data) > 0:
            cur.execute ("""
                UPDATE movies
                SET year=%s, title=%s, release_date=%s, director=%s, actor=%s, rating=%s
                WHERE title=%s
                """, (f['year'], f['title'], f['release_date'], f['director'], f['actor'], f['rating'], f['title']))
            cnx.commit()
            return render_template('index.html', message=['Movie {} successfully updated'.format(f['title'])])
        else:
            return render_template('index.html', message=['Movie with {} does not exist'.format(f['title'])])
    except Exception as e:
        return render_template('index.html', message=['Movie {} could not be updated - {}'.format(f['title'], e)])
    finally:
        cur.close()

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    cnx = getConnection()
    cur = cnx.cursor()
    f = request.form
    try:
        cur.execute("SELECT * FROM movies WHERE title=%s", (f['delete_title'],))
        data = cur.fetchall()
        if len(data) > 0:
            cur.execute ("""
                DELETE FROM movies
                WHERE title=%s
                """, (f['delete_title'],))
            cnx.commit()
            return render_template('index.html', message=['Movie {} successfully deleted'.format(f['delete_title'])])
        else:
            return render_template('index.html', message=['Movie with {} does not exist'.format(f['delete_title'])])
    except Exception as e:
        return render_template('index.html', message=['Movie {} could not be deleted - {}'.format(f['delete_title'], e)])
    finally:
        cur.close()

@app.route('/search_movie', methods=['GET'])
def search_movie():
    cnx = getConnection()
    cur = cnx.cursor()
    f = request.args
    try:
        cur.execute ("SELECT title, year, actor FROM movies WHERE actor=%s", (f.get('search_actor'),))
        data = cur.fetchall()
        message = [str(tup[0]) + ", " + str(tup[1]) + ", " +  str(tup[2]) for tup in data]
        #message = ' - '.join(arr)
        return render_template('index.html', message=message)
    except Exception as e:
        return render_template('index.html', message=['No movies found for actor {} - {}'.format(f.get('search_actor'), e)])
    finally:
        cur.close()

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    cnx = getConnection()
    cur = cnx.cursor()
    try:
        cur.execute ("SELECT title, year, actor, director, rating FROM movies WHERE rating=( SELECT MAX(rating) FROM movies)")
        data = cur.fetchall()
        message = [str(tup[0]) + ", " + str(tup[1]) + ", " +  str(tup[2])+ ", " +  str(tup[3]) + ", " +  str(tup[4]) for tup in data]
        #message = ' - '.join(arr)
        return render_template('index.html', message=message)
    except Exception as e:
        return render_template('index.html', message=['Highest rated movie could not be found - {}'.format(e)])
    finally:
        cur.close()

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    cnx = getConnection()
    cur = cnx.cursor()
    try:
        cur.execute ("SELECT title, year, actor, director, rating FROM movies WHERE rating=( SELECT MIN(rating) FROM movies)")
        data = cur.fetchall()
        message = [str(tup[0]) + ", " + str(tup[1]) + ", " +  str(tup[2])+ ", " +  str(tup[3]) + ", " +  str(tup[4]) for tup in data]
        #message = ' - '.join(arr)
        return render_template('index.html', message=message)
    except Exception as e:
        return render_template('index.html', message=['Lowest rated movie could not be found - {}'.format(e)])
    finally:
        cur.close()


@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html', message=['---'])


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
